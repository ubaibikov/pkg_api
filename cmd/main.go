package main

import (
	"io/ioutil"

	"bitbacket.org/ubaibikov/pkg_api/configs"
	"bitbacket.org/ubaibikov/pkg_api/internal/app/apiserver"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	configPath = "../configs/config.yaml"
)

var config []byte

func init() {
	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		logrus.Fatal(err)
	}
	config = data
}

func main() {

	var cnf configs.Config
	if err := yaml.Unmarshal(config, &cnf); err != nil {
		logrus.Fatal(err)
	}

	apiserver.Start(cnf)
}
