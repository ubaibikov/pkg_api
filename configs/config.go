package configs

// Config ...
type Config struct {
	Port string
	Tick int
	DB   struct {
		Port     string `yaml:"port"`
		Name     string `yaml:"name"`
		SSLModel string `yaml:"ssl_mode"`
		Password string `yaml:"password"`
		Host     string `yaml:"host"`
		User     string `yaml:"user"`
	} `yaml:"db"`
	API struct {
		Link string `yaml:"link"`
		Key  string `yaml:"key"`
	} `yaml:"api"`
}
