package apiserver

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbacket.org/ubaibikov/pkg_api/configs"
	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/httptransport"
	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/repository"
	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/service"
	"bitbacket.org/ubaibikov/pkg_api/pkg/currecny"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

// Start ...
func Start(cnf configs.Config) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("user=%s dbname=%s password=%s port=%s host=%s sslmode=disable",
		cnf.DB.User, cnf.DB.Name, cnf.DB.Password, cnf.DB.Port, cnf.DB.Host))
	if err != nil {
		logrus.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		logrus.Fatal(err)
	}

	mux := http.NewServeMux()

	// currency
	repository := repository.New(db)
	service := service.New(repository)

	mux.Handle("/api/currency", httptransport.NewHTPPServer(service))
	srv := new(server)

	rootCtx, rootCancel := context.WithCancel(context.Background())

	curencyClient := currecny.New(&http.Client{}, cnf.API.Link, cnf.API.Key)

	go service.UpdateAll(rootCtx, cnf.Tick, curencyClient)

	go func() {
		if err = srv.run(cnf.Port, mux); err != nil {
			logrus.Fatal(err)
		}
	}()

	logrus.Info("app started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	rootCancel()

	if err := srv.shutdown(rootCtx); err != nil {
		logrus.Errorf("server shutted down %s", err.Error())
	}

	if err := db.Close(); err != nil {
		logrus.Errorf("error in db.Close: %s", err.Error())
	}
}

type server struct {
	httpServer *http.Server
}

// Run ...
func (s *server) run(port string, handler *http.ServeMux) error {
	s.httpServer = &http.Server{
		Addr:           ":" + port,
		MaxHeaderBytes: 1 << 20,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		Handler:        handler,
	}

	return s.httpServer.ListenAndServe()
}

// Shutdown ...
func (s *server) shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
