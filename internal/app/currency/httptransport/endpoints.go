package httptransport

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type createCurrencyForm struct {
	Cur1 string `json:"currency1" binding:"required"`
	Cur2 string `json:"currency2" binding:"required"`
}

func (s *server) createEndpoint(c *gin.Context) {

	var currency createCurrencyForm

	c.BindJSON(&currency)

	c.JSON(http.StatusCreated, gin.H{
		"success_message": "Валютная пара создана!",
		"id":              currency,
	})
}

type query struct {
	Cur1 string `form:"currencyFrom" json:"currencyFrom"`
	Cur2 string `form:"currencyTo" json:"currencyTo"`
}

func (s *server) convertEndpoint(c *gin.Context) {
	var query query
	c.Bind(&query)

	c.JSON(http.StatusOK, gin.H{
		"success_message": "пеервод валют",
		"data":            query.Cur1 + "_" + query.Cur2,
	})
}
