package httptransport

import (
	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/service"
	"github.com/gin-gonic/gin"
)

type server struct {
	srv service.Service
}

// NewHTPPServer ...
func NewHTPPServer(srv service.Service) *gin.Engine {
	server := &server{
		srv: srv,
	}
	r := gin.Default()

	r.POST("currency/crete", server.createEndpoint)
	r.GET("currency/", server.convertEndpoint)

	return r
}
