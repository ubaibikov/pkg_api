package repository

import (
	"time"

	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/domain"
)

var (
	id       int
	currency domain.Currency
)

func (r *repository) Create(cur1, cur2 string) int {
	r.db.QueryRow("insert into currency (cur1, cur2, updated_at) values($1,$2,$3) returning id",
		cur1, cur2, time.Now()).Scan(&id)

	return id
}

func (r *repository) Find(cur1, cur2 string) *domain.Currency {
	r.db.QueryRowx("select * from currency where cur1=$1 and cur2=$2", cur1, cur2).StructScan(&currency)

	return &currency
}
func (r *repository) Update(cur1, cur2, cource string) int {
	r.db.QueryRowx(`update currency set cource=$1, updated_at=$2 where cur1=$3 and cur2=$4 returning id`,
		cource, time.Now(), cur1, cur2).Scan(&id)
	return id
}

func (r *repository) GetAll() []domain.Currency {
	curs := make([]domain.Currency, 0)
	r.db.Select(&curs, "select * from currency")

	return curs
}
