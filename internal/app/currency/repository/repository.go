package repository

import (
	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/domain"
	"github.com/jmoiron/sqlx"
)

// Repository ...
type Repository interface {
	Create(cur1, cur2 string) int
	Find(cur1, cur2 string) *domain.Currency
	Update(cur1, cur2, cource string) int
	GetAll() []domain.Currency
}

// Repository ...
type repository struct {
	db *sqlx.DB
}

// New ...
func New(db *sqlx.DB) Repository {
	return &repository{
		db: db,
	}
}
