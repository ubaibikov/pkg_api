package service

import (
	"context"
	"fmt"
	"time"

	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/domain"
	"bitbacket.org/ubaibikov/pkg_api/pkg/currecny"
	"github.com/sirupsen/logrus"
)

func (s *service) GetAll() []domain.Currency {
	return s.repo.GetAll()
}

func (s *service) Convert(cur1, cur2 string) *domain.Currency {
	return s.repo.Find(cur1, cur2)
}

func (s *service) UpdateCource(cur1, cur2, cource string) int {
	return s.repo.Update(cur1, cur2, cource)
}

func (s *service) UpdateAll(ctx context.Context, tick int, currency *currecny.Currency) {
	for {
		select {
		case <-ctx.Done():
			logrus.Info("usecase: currecny close")
			return
		case <-time.After(time.Duration(tick) * time.Second):
			logrus.Info("update")
			for _, data := range s.GetAll() {
				cource, err := currency.ConvertPairs(data.Currency1, data.Currency2)
				if err != nil {
					logrus.Fatal(err)
					return
				}

				s.UpdateCource(data.Currency1, data.Currency2, fmt.Sprintf("%f", cource))
			}
		}
	}
}
