package service

import (
	"context"

	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/domain"
	"bitbacket.org/ubaibikov/pkg_api/internal/app/currency/repository"
	"bitbacket.org/ubaibikov/pkg_api/pkg/currecny"
)

// Service ...
type Service interface {
	GetAll() []domain.Currency
	UpdateCource(cur1, cur2, cource string) int
	Convert(cur1, cur2 string) *domain.Currency
	UpdateAll(ctx context.Context, tick int, currency *currecny.Currency)
}

type service struct {
	repo repository.Repository
}

// New ...
func New(r repository.Repository) Service {
	return &service{
		repo: r,
	}
}
