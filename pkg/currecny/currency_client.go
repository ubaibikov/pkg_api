package currecny

import (
	"encoding/json"
	"net/http"
)

// Currency ...
type Currency struct {
	client *http.Client
	link   string
	key    string
}

// New ...
func New(client *http.Client, link, key string) *Currency {
	return &Currency{
		client: client,
		link:   link,
		key:    key,
	}
}

// ConvertPairs ...
func (c *Currency) ConvertPairs(currecnyTo, currecnyFrom string) (float64, error) {
	pairs := currecnyTo + "_" + currecnyFrom
	request, err := http.NewRequest("GET", c.link+pairs+"&compact=ultra&apiKey="+c.key, nil)
	if err != nil {
		return 0, err
	}

	resp, err := c.client.Do(request)
	if err != nil {
		return 0, err
	}

	var result map[string]float64

	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return 0, err
	}

	return result[pairs], nil
}
